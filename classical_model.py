import csv
import pandas as pd
import string
import keras
import tensorflow 
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from keras.models import Model, Sequential
from keras.layers import GRU, Input, Dense, TimeDistributed, Activation, RepeatVector, Bidirectional, Dropout, LSTM
from tensorflow.keras.layers import Embedding
from keras.optimizers import Adam
from keras.losses import sparse_categorical_crossentropy

import collections
import string
import numpy as np
import pandas as pd
from keras.preprocessing.text import Tokenizer

data = pd.read_csv("C:\IA\pramit\\nouveau_fichier.csv", sep=',', quotechar='"',encoding='UTF-8')
data[['eng', 'fr']] = data['eng'].str.split('",',n=1,expand=True)
data.dropna(subset=['fr'],inplace=True)

def separate_punctuation(text):
   
    punctuation = string.punctuation
    
  
    for char in punctuation: 
    
        text = text.replace(char, f' {char} ')
    
  
    text = ' '.join(text.split())
    
    return text

eng=[word for sentence in data['eng'] for word in separate_punctuation(sentence).split()]
fr=[word for sentence in data['fr'] for word in separate_punctuation(sentence).split()]

max_length = 0

for phrase in data['fr']:
        phrase=separate_punctuation(phrase)
        length = len(phrase)  # ou len(phrase.split()) pour compter les mots au lieu des caractères

        if length > max_length:
            max_length = length

max_french_sequence_length=max_length


len_eng=len(eng)
len_fr=len(fr)
def logits_to_text(logits, tokenizer):

    index_to_words = {id: word for word, id in tokenizer.word_index.items()}
    index_to_words[0] = '<PAD>'

    return ' '.join([index_to_words[prediction] for prediction in np.argmax(logits, 1)])
    
english_counter = collections.Counter([word for sentence in data['eng'] for word in sentence.split()])
french_counter = collections.Counter([word for sentence in data['fr'] for word in sentence.split()])

print('10 Most common words in the English dataset:')
print('"' + '" "'.join(list(zip(*english_counter.most_common(10)))[0]) + '"')
print('10 Most common words in the French dataset:')
print('"' + '" "'.join(list(zip(*french_counter.most_common(10)))[0]) + '"')

token = Tokenizer()
token.fit_on_texts(data['fr'])
damn1=token.texts_to_sequences(data['fr'])
french_tokenizer=token
token.fit_on_texts(data['eng'])
damn2=token.texts_to_sequences(data['eng'])
english_tokenizer=token

damn1=pad_sequences(damn1, maxlen=None, padding='post')
damn2=pad_sequences(damn2, maxlen=None, padding='post')

def simple_model(input_shape, output_sequence_length, english_vocab_size, french_vocab_size):
    
    learning_rate = 0.005
    
    model = Sequential()
    model.add(GRU(256, input_shape=input_shape[1:], return_sequences=True))
    model.add(TimeDistributed(Dense(1024, activation='relu')))
    model.add(Dropout(0.5))
    model.add(TimeDistributed(Dense(french_vocab_size, activation='softmax'))) 

    # Compile model
    model.compile(loss=sparse_categorical_crossentropy,
                  optimizer=Adam(learning_rate),
                  metrics=['accuracy'])
    return model


# Reshaping the input to work with a basic RNN
tmp_x = np.pad(damn2, ((0, 0), (0, max_french_sequence_length - damn2.shape[1])))
tmp_x = tmp_x.reshape((-1, tmp_x.shape[1], 1))

# Train the neural network
simple_rnn_model = simple_model(
    tmp_x.shape,
    max_french_sequence_length,
    len_eng,
    len_fr)

print(simple_rnn_model.summary())

simple_rnn_model.fit(tmp_x, damn1, batch_size=1024, epochs=10, validation_split=0.2)
